package main.java.com.app;

import java.util.Date;
import java.util.Calendar;

public class HappyProgrammersDay {

	public static void main(String args[]) {

		Date date = getDateWithoutTimeUsingCalendar();
		long time = date.getTime();

		if (isProgrammersDay(time)) {
			System.out.println("\tHappy programmers day! =)");
		} else {
			System.out.println("Simple day =(");
		}

	}

	private static boolean isProgrammersDay(long time) {
		final long programersDayDateTime = getDateWithoutTimeUsingCalendar(13, 9, 2019).getTime();
		final long programersDayDateLeapYearTime = getDateWithoutTimeUsingCalendar(12, 9, 2019).getTime();

		if (isLeapYear()) {
			return time == programersDayDateLeapYearTime;
		}

		return time == programersDayDateTime;
	}

	private static Date getDateWithoutTimeUsingCalendar() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	private static Date getDateWithoutTimeUsingCalendar(int day, int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.MONTH, month -1);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static boolean isLeapYear() {
		Calendar cal = Calendar.getInstance();
		return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
	}
}
